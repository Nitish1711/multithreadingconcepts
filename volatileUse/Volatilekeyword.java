package com.volatileUse;

public class Volatilekeyword {

	public Volatilekeyword() {
		// TODO Auto-generated constructor stub
	}
	
/*	Each thread has its own stack, and so its own copy of variables it can access. When the thread is created, it copies the 
	value of all accessible variables in its own memory. The volatile keyword is used to say to the 
	jvm "Warning, this variable may be modified in an other Thread". Without this keyword the JVM is free to 
	make some optimizations, like never refreshing those local copies in some threads. The volatile force the 
	thread to update the original variable for each variable. 
	The volatile keyword could be used on every kind of variable, either primitive or objects.*/
	
    private static  volatile int MY_INT = 0;

	public static void main(String[] args) {
		 new Listener().start();
		 new ChangeMaker().start();
	}
	
	static class Listener extends Thread{
		 
		public void run(){
			int localVal=MY_INT;
			System.out.println("existing my int val "+MY_INT);
			while(localVal<5){
				if(localVal!=MY_INT){
					System.out.println("new my int val "+MY_INT);
					localVal=MY_INT;
				}
			}
			
		}
	}
	
	
	static class ChangeMaker extends Thread{
        @Override
        public void run() {

            int local_value = MY_INT;
            while (MY_INT <5){
               
                MY_INT = ++local_value;
                System.out.println("Incrementing MY_INT to "+ MY_INT);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) { e.printStackTrace(); }
            }
        }
	}

}
