package com.blockingQueue;

import java.util.concurrent.BlockingQueue;

public class Prod implements Runnable{

    protected BlockingQueue queue = null;

    public Prod(BlockingQueue queue) {
        this.queue = queue;
    }

    public void run() {
        try {
        	int i=1;
        	while(i<10){
        		 queue.put(i);
                 Thread.sleep(1000);
                 i++;
                 System.out.println("current queue size "+queue.size()+" thread waitin");
                 
        	 }
          } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
