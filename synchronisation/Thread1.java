package com.synchronisation;

public class Thread1 extends Thread{
	Utilizingclass ul;

	public Thread1(Utilizingclass ul) {
		 this.ul=ul;
	}
	
	@Override
	public void run() {
		// ul.print(10); //for object lock sync
		 Utilizingclass.print(10);// for static sync
		Thread.yield();
	}

	 
}
