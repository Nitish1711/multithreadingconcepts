public class MultiThreadPrintNumber {
  int i = 1;

  public synchronized void printNumber(String threadNm) throws InterruptedException{

      if(threadNm.equals("t1")){
        if(i%2 == 1){
          System.out.println(Thread.currentThread().getName()+"--"+ i++);
          notify();
        } else {
        	System.out.println(Thread.currentThread().getName()+" waiting");
          wait();
        }
      } else if(threadNm.equals("t2")){
        if(i%2 == 0){
          System.out.println(Thread.currentThread().getName()+"--"+ i++);
          notify();
        } else {
        	System.out.println(Thread.currentThread().getName()+" waiting");

          wait();
        }
      }

    }

  public static void main(String[] args) {
   MultiThreadPrintNumber obj = new MultiThreadPrintNumber();
   Thread t1=new Thread(new Oddthread(obj));
   Thread t2=new Thread(new EvenThread(obj));

  // Oddthread t1=new Oddthread(obj);
   //EvenThread t2=new EvenThread(obj);

    t1.setName("t1");
    t2.setName("t2");
    t1.start();
    t2.start();
  }
}
