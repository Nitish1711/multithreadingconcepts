package com.cyclicBarrier;

import java.util.concurrent.CyclicBarrier;

/*The threads wait for each other by calling the await() method on the CyclicBarrier. 
Once N threads are waiting at the CyclicBarrier, all threads are released and can continue running

The waiting threads waits at the CyclicBarrier until either:

The last thread arrives (calls await() )
The thread is interrupted by another thread (another thread calls its interrupt() method)
Another waiting thread is interrupted
Another waiting thread times out while waiting at the CyclicBarrier
The CyclicBarrier.reset() method is called by some external thread.
CyclicBarrier Action

The CyclicBarrier supports a barrier action, which is a Runnable
that is executed once the last thread arrives. You pass the Runnable 
4barrier action to the CyclicBarrier in its constructor*/

public class MainClass {

public static void main(String[] args) {
	Runnable barrier1Action = new Runnable() {
	    public void run() {
	        System.out.println("BarrierAction 1 executed ");
	    }
	};
	Runnable barrier2Action = new Runnable() {
	    public void run() {
	        System.out.println("BarrierAction 2 executed ");
	    }
	};

	CyclicBarrier barrier1 = new CyclicBarrier(2, barrier1Action);
	CyclicBarrier barrier2 = new CyclicBarrier(2, barrier2Action);

	RunningThread barrierRunnable1 =
	        new RunningThread(barrier1, barrier2);

	RunningThread barrierRunnable2 =
	        new RunningThread(barrier1, barrier2);

	new Thread(barrierRunnable1).start();
	new Thread(barrierRunnable2).start();
}

}
