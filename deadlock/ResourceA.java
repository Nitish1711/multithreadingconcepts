package com.deadlock;

public class ResourceA extends Thread {

	public void run(){
		synchronized (String.class) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println(Thread.currentThread().getName() + "has acquired lock "
                    + "on String class and waiting to acquire lock on Object class...");
			
			synchronized (Object.class) {
				System.out.println(Thread.currentThread().getName() +
                        " has acquired lock on Object class");
			}
			
		}
	}
	 
}
