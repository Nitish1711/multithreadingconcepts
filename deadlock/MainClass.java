package com.deadlock;

public class MainClass {

	public MainClass() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		 Thread thread1 = new Thread(new ResourceA(), "ResourceA");
         Thread thread2 = new Thread(new ResourceB(), "ResourceB");
         thread1.start();
        /*     use join to avoid deadlock try {
			thread1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
         thread2.start();

	}

}
