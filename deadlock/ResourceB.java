package com.deadlock;

public class ResourceB extends Thread {
	public void run(){
		/* synchronized (Object.class) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println(Thread.currentThread().getName() + "has acquired lock "
                    + "on Object class and waiting to acquire lock on String class...");
			
			synchronized (String.class) {
				System.out.println(Thread.currentThread().getName() +
                        " has acquired lock on String class");
			}
			
		}  */
		
		/*comment out above and use this avoid deadlock*/
	 	synchronized (String.class) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println(Thread.currentThread().getName() + "has acquired lock "
                    + "on Object class and waiting to acquire lock on String class...");
			
			synchronized (Object.class) {
				System.out.println(Thread.currentThread().getName() +
                        " has acquired lock on String class");
			}
			
		} 
		
	}
}
