package com.simpleThread;

public class ThreadClass implements Runnable {
	
	public int nVal;
	
	public ThreadClass(int n) {
		 this.nVal=n;
	}

	public void run(){
		for(int i=1;i<=5;i++){ 
			System.out.println("current nval is  "+nVal);
			System.out.println(" updated value "+nVal*i);
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {   //sleep interpretation
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
		
	}
	 

	public static void main(String[] args) {
		ThreadClass obj1=new ThreadClass(10);
		 
		ThreadClass obj2=new ThreadClass(20);
		 
		Thread t1=new Thread(obj1);
		//t1.yield();
		/*try {
			t1.join();
		} catch (InterruptedException e) {                           //join interpretation
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		Thread t2=new Thread(obj2);

		t1.start();
		t2.start();
		

	}

}
