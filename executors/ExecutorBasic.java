package com.executors;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorBasic {

	public ExecutorBasic() {
		// TODO Auto-generated constructor stub
	}
	
	
	/*execute(Runnable)

	The execute(Runnable) method takes a java.lang.Runnable object,
	and executes it asynchronously. Here is an example of executing a Runnable with an ExecutorService:
		There is no way of obtaining the result of the executed Runnable, if necessary. 
		4You will have to use a Callable for that
	*/
	
	/*public static void main(String[] args) {
		
		

		executorService.execute(new Runnable() {
		    public void run() {
		        System.out.println("Asynchronous task");
		    }
		});

		executorService.shutdown();

		
	}*/
	
	
	/*submit(Runnable)

	The submit(Runnable) method also takes a Runnable implementation, 
	but returns a Future object. This Future object can be used to check if the Runnable as finished executing.*/


	
/*public static void main(String[] args) {
	
	ExecutorService executorService = Executors.newSingleThreadExecutor();

		
	@SuppressWarnings("rawtypes")
	Future future = executorService.submit(new Runnable() {
	    public void run() {
	        System.out.println("Asynchronous task");
	    }
	});

	try {
		System.out.println("future.get(); "+future.get());
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ExecutionException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  //returns null if the task has finished correctly.


		
	}*/
	
	
	/*The submit(Callable) method is similar to the submit(Runnable)
	method except for the type of parameter it takes. The Callable instance is very similar 
	to a Runnable except that its call() method can return a result. The Runnable.run() method cannot return a result.

			The Callable's result can be obtained via the Future object returned by the submit(Callable) method. 
*/	
	/*public static void main(String[] args) {
		
		ExecutorService executorService = Executors.newSingleThreadExecutor();

			
		Future future = executorService.submit(new Callable(){
		    public Object call() throws Exception {
		        System.out.println("Asynchronous Callable");
		        return "Callable Result";
		    }
		});

		try {
			System.out.println("future.get() = " + future.get());
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

			

	}*/
	
	
/*	invokeAny()

	The invokeAny() method takes a collection of Callable objects, or subinterfaces of Callable. 
	Invoking this method does not return a Future, but returns the result of one of the Callable objects.
			You have no guarantee about which of the Callable's results you get. Just one of the ones that finish.

	If one of the tasks complete (or throws an exception), the rest of the Callable's are cancelled.
*/

	
	
/*public static void main(String[] args) {
		
	ExecutorService executorService = Executors.newSingleThreadExecutor();

	Set<Callable<String>> callables = new HashSet<Callable<String>>();

	callables.add(new Callable<String>() {
	    public String call() throws Exception {
	        return "Task 1";
	    }
	});
	callables.add(new Callable<String>() {
	    public String call() throws Exception {
	        return "Task 2";
	    }
	});
	callables.add(new Callable<String>() {
	    public String call() throws Exception {
	        return "Task 3";
	    }
	});

	String result=null;
	try {
		result = executorService.invokeAny(callables);
	} catch (InterruptedException | ExecutionException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	System.out.println("result = " + result);

	executorService.shutdown();
 }*/

	
	/*invokeAll()

	The invokeAll() method invokes all of the Callable objects you pass to it
in the collection passed as parameter. The invokeAll() returns a list of Future objects 
via which you can obtain the results of the executions of each Callable.

	Keep in mind that a task might finish due to an exception, so it may not
	have "succeeded". There is no way on a Future to tell the difference*/

	public static void main(String[] args) {
		
		ExecutorService executorService = Executors.newSingleThreadExecutor();

		Set<Callable<String>> callables = new HashSet<Callable<String>>();

		callables.add(new Callable<String>() {
		    public String call() throws Exception {
		        return "Task 1";
		    }
		});
		callables.add(new Callable<String>() {
		    public String call() throws Exception {
		        return "Task 2";
		    }
		});
		callables.add(new Callable<String>() {
		    public String call() throws Exception {
		        return "Task 3";
		    }
		});

		List<Future<String>> futures=null;
		try {
			futures = executorService.invokeAll(callables);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(Future<String> future : futures){
		    try {
				System.out.println("future.get = " + future.get());
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		executorService.shutdown();

	 }


}
