package com.threaadfive;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

public class FiveThreadsmain {
	
	 AtomicInteger  i=new AtomicInteger(1);
	 int t1Current=0;
	 int t2Current=0;
	 int t3Current=0;
	 
	 
	 public synchronized void show(){
		 
		 if(Thread.currentThread().getName().equals("t1")){
			if(i.get()<3){
			 System.out.println(Thread.currentThread().getName()+" printing "+i.get() );
			 i.incrementAndGet();
			 notify();
			}else{
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		 }
			
			 if(Thread.currentThread().getName().equals("t2")){
					if(i.get()<3){
					 System.out.println(Thread.currentThread().getName()+" printing "+i.get() );
					 i.incrementAndGet();
					 notify();
					}else{
						try {
							wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
			 }
					
					
					 if(Thread.currentThread().getName().equals("t3")){
							if(i.get()<3){
							 System.out.println(Thread.currentThread().getName()+" printing "+i.get() );
							 i.incrementAndGet();
							 notify();
							}else{
								try {
									wait();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
			
		 
					 }
			 
		 
		 
	 }

	public static void main(String[] args) {
		Runnable barrieraction=new Runnable() {
			
			@Override
			public void run() {
			System.out.println("Barrier executed");
				
			}
		};
		
		CyclicBarrier barrier1=new CyclicBarrier(3,barrieraction );
		FiveThreadsmain fth=new FiveThreadsmain();
		Thread1 t1=new Thread1(barrier1,fth);
		Thread2 t2=new Thread2(barrier1,fth);
		Thread3 t3=new Thread3(barrier1,fth);
t1.setName("t1");
t2.setName("t2");
t3.setName("t3");
		t1.start();
		t2.start();
		t3.start();
		
	}
	
}
