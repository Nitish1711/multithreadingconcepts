package com.yield;

public class Yieldmainthread {

	public static void main(String[] args)
	   {
	      ProducerYield producer = new ProducerYield();
	      Consumeryield consumer = new Consumeryield();
	       
	      producer.setPriority(Thread.MAX_PRIORITY); //Min Priority
	      consumer.setPriority(Thread.MIN_PRIORITY); //Max Priority
	       
	      producer.start();
	      consumer.start();
	   }

}
