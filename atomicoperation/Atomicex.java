package com.atomicoperation;

public class Atomicex {

/*	Atomic operations are performed in a single unit of task without
	interference from other operations. Atomic operations are necessity in 
	multi-threaded environment to avoid data inconsistency.*/


	   public static void main(String[] args) throws InterruptedException {
		   
	        ProcessingThread pt = new ProcessingThread();
	        Thread t1 = new Thread(pt, "t1");
	        t1.start();
	        Thread t2 = new Thread(pt, "t2");
	        t2.start();
	        t1.join();
	        t2.join();
	        System.out.println("Processing count=" + pt.getCount());
	    }

}
