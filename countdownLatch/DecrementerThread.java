package com.countdownLatch;

import java.util.concurrent.CountDownLatch;

public class DecrementerThread implements Runnable{
	CountDownLatch latch=null;

	public DecrementerThread(CountDownLatch latch) {
		 this.latch = latch;
	}
	
	 public void run() {

	        try {
	            Thread.sleep(1000);
	            this.latch.countDown();
	            System.out.println("Latch current val "+this.latch.getCount());

	            Thread.sleep(1000);
	            this.latch.countDown();
	            System.out.println("Latch current val "+this.latch.getCount());

	            Thread.sleep(1000);
	            this.latch.countDown();
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	    }


}
