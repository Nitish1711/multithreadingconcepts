package com.countdownLatch;

import java.util.concurrent.CountDownLatch;

public class WaiterThread implements Runnable {

	CountDownLatch latch = null;

    public WaiterThread(CountDownLatch latch) {
        this.latch = latch;
    }

    public void run() {
        try {
        	 System.out.println("Latch current val "+this.latch.getCount());
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Waiter Released");
    }
	

}
