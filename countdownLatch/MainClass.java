package com.countdownLatch;

import java.util.concurrent.CountDownLatch;

/* java.util.concurrent.CountDownLatch is a concurrency construct that 
allows one or more threads to wait for a given set of operations to complete.

A CountDownLatch is initialized with a given count. 
This count is decremented by calls to the countDown() method. Threads waiting for this count to reach zero 
can call one of the await() methods. Calling await() blocks the thread until the count reaches zero.*/
public class MainClass {

	 public static void main(String[] args) {
		
	
	 CountDownLatch latch = new CountDownLatch(3);
     WaiterThread      waiter      = new WaiterThread(latch);
	 DecrementerThread decrementer = new DecrementerThread(latch);

	new Thread(waiter).start();
	new Thread(decrementer).start();

	try {
		Thread.sleep(4000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
}