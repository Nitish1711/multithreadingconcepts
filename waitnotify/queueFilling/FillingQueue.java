package com.waitnotify.queueFilling;

import java.util.Random;
import java.util.Vector;

public class FillingQueue extends Thread {
	
	public Vector<Integer> elementsList=new Vector<Integer>();
	public static int randomInt=0;
	public   final Random randomGenerator = new Random();
	

 
	
	public FillingQueue() {
		// TODO Auto-generated constructor stub
	}
	

	public  void run(){
		 while (true) {
	    	  try {
				fillElements();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		  
	  }
	
	public synchronized void fillElements() throws InterruptedException{
    System.out.println("current randomInt value  "+randomInt);
  
    randomInt=randomGenerator.nextInt(1000);
	 
	 while(randomInt==0){
		    wait();
			System.out.println("QUeue is qIING!");
		}
    
    System.out.println("QUeue has started filling  elements! "+randomInt);
    
    if(elementsList.size()>0){
		System.out.println("thread waiting ");
		wait();
	}
    
		elementsList.add(randomInt);
		
		
		
		System.out.println("Notifying thread S");
		  notify();
		
	}
	
	public synchronized void getElementsFromQueue() throws InterruptedException{
		
		while(elementsList.size()==0){
			System.out.println("QUeue is waiting !");
			 wait();
		 }
		int queueMessage=(Integer)elementsList.firstElement();
		System.out.println("FOUND ELEMENT ***** "+queueMessage+" ***** and removing it");
		elementsList.removeElement(queueMessage);
		notify();
		System.out.println("ELEMENT "+queueMessage+" removed !");
	}
	
	
	
}
