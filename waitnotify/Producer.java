package com.waitnotify;

import java.util.Date;
import java.util.Vector;

public class Producer extends Thread {

 public static final int MAX_SIZE_QUEUE=5;
 public Vector messages=new Vector();
 
 @Override
 public void run() {
     while (true) {
    	 
	     putMessage();
	     //sleep(5000);
	 }
 }
 
 //the thread which i working will acqyire lock on thi method and as soon as this will rach to wait the progesion top intantly and then anothe thread come and notify that another thread after 
 //complting thi task
 
 private synchronized void putMessage(){
	 System.out.println("condition met");
	 while(messages.size()==MAX_SIZE_QUEUE){
		 try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
		 messages.addElement(new Date().toString());
		 System.out.println("putting messsage ");
		notify();
	 
 }
 
 public synchronized String getMessage() throws InterruptedException {
     notify();
     while (messages.size() == 0) {
         wait();//By executing wait() from a synchronized block, a thread gives up its hold on the lock and goes to sleep.
     }
     String message = (String) messages.firstElement();
     messages.removeElement(message);
     return message;
 }

}
