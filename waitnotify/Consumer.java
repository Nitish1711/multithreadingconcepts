package com.waitnotify;

public class Consumer extends Thread {
	
	Producer prod;
	
	public Consumer(Producer p) {
		// TODO Auto-generated constructor stub
		this.prod=p;
		
	}
	
	 @Override
	    public void run() {
	        try {
	            while (true) {
	                String message = prod.getMessage();
	                System.out.println("Got message: " + message);
	                
	            }
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	    }
	 

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			Producer p=new Producer();
			p.start();
			new Consumer(p).start();
			

	}

}
